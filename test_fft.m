fs = 1000;
t = (0:512)/fs; % time interval of 5 seconds
y = sin(2*pi*5*t) + sin(2*pi*15*t);
Y = fft(y,1024);
fhz = linspace(0,1000,1024); % frequencies returned by fft
plot(fhz,abs(Y));



% clear all; close all;
% 
% % load('neuro_act.mat');
% fs = 1000;
% t = (0:1*60*1000)/fs; % time interval of 1 minute
% t1 = sin(2*pi*5*t);
% 
% % t1 = t(:,1);
% 
% % t1 = t1 - mean(t1);
% 
% fs = 1000;
% n1ms = fs/1000;
% frsize = 500*n1ms;
% frshift = 25*n1ms;
% frovlap = frsize - frshift;
% 
% nfft = 1024;
% nfftby2 = round(nfft/2 + 1);
% hfhz = linspace(0,fs/2,nfftby2);
% 
% % make frames and window the signal
% t1b = buffer(t1,frsize,frovlap,'nodelay');
% t1bw = bsxfun(@times,t1b,hamming(frsize));
% nof = size(t1b,2)
% 
% nif = 1000;
% t1b = t1b(:,1:nif);
% t1bw = t1bw(:,1:nif);
% 
% % compute spectrum 
% magy_t1b = abs(fft(t1b,nfft));
% magy_t1bw = abs(fft(t1bw,nfft));
% 
% % half magnitude spectrum
% hmagy_t1b = magy_t1b(1:nfftby2,:);
% hmagy_t1bw = magy_t1bw(1:nfftby2,:);
% 
% figure();
% for i = 1:nof
%     subplot(411);
%     plot(t1b(:,i));
%     subplot(412);
%     plot(hfhz,hmagy_t1b(:,i));  
%     subplot(413);
%     plot(t1bw(:,i));
%     subplot(414);
%     plot(hfhz,hmagy_t1bw(:,i));
%     pause
% end
% 
% 
% 
% % % power spectrum 
% % pspec = 20*log10(hmagy_t1b);
% % pspec_w = 20*log10(hmagy_t1bw);
% % 
% % % power in the interseted sub-bands
% % p_sb1 = mean(pspec_w(six1:fix1,:)); 
% % 
% % 
% % h1 = figure(1);
% % plot(t1);
% % 
% % % h2 = figure(2);
% % % subplot(411); surf(magy_t1b(1:nfftby2,:),'edgecolor','none'); axis tight; view(0,90); 
% % % subplot(412); surf(magy_t1bw(1:nfftby2,:),'edgecolor','none'); axis tight; view(0,90); 
% % % subplot(413); surf(20*log10(magy_t1b(1:nfftby2,:)),'edgecolor','none'); axis tight; view(0,90); 
% % % subplot(414); surf(20*log10(magy_t1bw(1:nfftby2,:)),'edgecolor','none'); axis tight; view(0,90); 
% % % 
% % % disp('paused press any key to continue .... ');
% % % pause
% % %  
% % 
% % h3 = figure(3);
% % 
% % for i = 1:nof
% % 
% % nr = 4; nc = 1; k = 1;    
% % subplot(nr,nc,k); k = k + 1; plot(t1b(:,i)); axis tight;
% % subplot(nr,nc,k); k = k + 1; plot(hfhz,magy_t1b(1:nfftby2,i)); axis tight;
% % subplot(nr,nc,k); k = k + 1; plot(t1bw(:,i)); axis tight;
% % subplot(nr,nc,k); k = k + 1; plot(hfhz,magy_t1bw(1:nfftby2,i)); axis tight;
% % 
% % pause
% % 
% % end
% 
